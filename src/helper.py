def many_decisions(arg):
    """Function that has different paths and returns a string"""
    assert isinstance(arg, int)

    ret = []
    if arg % 3 == 0:
        if arg % 2 == 0:
            ret.append('Fuzz')
        else:
            ret.append('Fizz')
    if arg % 5 == 0:
        ret.append('Buzz')
    return ''.join(ret) if ret else None
