from helper import many_decisions
import numpy as np


class Sandbox(object):

    def __init__(self, list_of_obj=list()):
        self._list_of_obj = list_of_obj

    def results_list(self, arg):
        """Function that returns a list"""
        ret = []
        for i in range(arg):
            res = many_decisions(i)
            if res:
                ret.append(res)
        return ret

    def results_dict(self, arg):
        """Function that returns a dictionary, also references list from init"""
        ret = dict()
        ret_list = self.results_list(arg)
        for f in ret_list:
            if f in self._list_of_obj:
                continue
            ret[f] = ret.get(f, 0) + 1
        return ret

    @staticmethod
    def numpy_test():
        """Function that returns np.array([ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. ])
        with some floating point differences"""
        return np.linspace(0.1, 1, 10)

