#import statements
import unittest
import hamcrest
from helper import many_decisions

#unit test class
class TestHelper(unittest.TestCase):

    #unit tests
    def test_fizzbuzz(self):
        self.assertEqual(many_decisions(15), 'FizzBuzz')

    def test_fuzzbuzz(self):
        self.assertEqual(many_decisions(30), 'FuzzBuzz')

    def test_exception(self):
        with self.assertRaises(AssertionError):
            many_decisions('nope')

    #pip install nose

    #nosetests
    #nosetests test/test_helper.py
    #..
    #----------------------------------------------------------------------
    #Ran 2 tests in 0.000s
    #
    #OK

    # F.
    # ======================================================================
    # FAIL: test_fizzbuzz (test_helper.TestHelper)
    # ----------------------------------------------------------------------
    # Traceback (most recent call last):
    #   File "/Users/juliechua/Engineering/unittest_sandbox/test/test_helper.py", line 11, in test_fizzbuzz
    #     self.assertEqual(many_decisions(15), 'FuzzBuzz')
    # AssertionError: 'FizzBuzz' != 'FuzzBuzz'
    #
    # ----------------------------------------------------------------------
    # Ran 2 tests in 0.001s
    #
    # FAILED (failures=1)
