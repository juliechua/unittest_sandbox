#import statements
import unittest
from sandbox import Sandbox
import numpy as np
import mock

#unit test class
class TestSandbox(unittest.TestCase):

    #setting up
    def setUp(self):     #per test
        # self.sandbox = Sandbox(['Fizz'])
        self.sandbox = Sandbox(['Fizz'])

    # @classmethod
    # def setUpClass(cls):     #per class
    #     cls.sandbox = Sandbox([5,7,9])

    #cleaning up
    def tearDown(self):
        pass

    # @classmethod
    # def tearDownClass(cls):
    #     pass

    #unit tests
    def test_results_dict(self):
        self.assertDictEqual(self.sandbox.results_dict(6), {'Buzz': 1, 'FuzzBuzz': 1})

    def test_numpy(self):
        np.testing.assert_almost_equal(self.sandbox.numpy_test(),[ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. ])
        # straightforward comparison fails
        # self.assertListEqual(self.sandbox.numpy_test().tolist(),[ 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1. ])

    @mock.patch('sandbox.many_decisions')
    def test_mock_helper_ret_val(self, mock_helper):
        mock_helper.return_value = 1
        res = self.sandbox.results_list(4)
        self.assertListEqual(res, [1, 1, 1, 1])

    @mock.patch('sandbox.many_decisions')
    def test_mock_helper_side_effect(self, mock_helper):
        mock_helper.side_effect = [1, 2, 3, 4]
        res = self.sandbox.results_list(4)
        self.assertListEqual(res, [1, 2, 3, 4])

    @mock.patch('sandbox.np.linspace')
    @mock.patch('sandbox.many_decisions')
    def test_mock_helper_multiple_check_calls(self, mock_helper, mock_linspace):
        mock_helper.return_value = 1
        mock_linspace.return_value = [1]
        res = self.sandbox.numpy_test()
        mock_helper.assert_not_called()
        mock_linspace.assert_called_once_with(0.1, 1, 10)

        res = self.sandbox.results_dict(2)
        self.assertEqual(mock_helper.call_count, 2)
        mock_helper.assert_has_calls([mock.call(0), mock.call(1)], any_order=False)


